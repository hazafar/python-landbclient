Name:       python-landbclient
Version:    0.19.1
Release:    1
Summary:    Python API and CLI for CERN's landb

Group:      Development/Languages
License:    ASL 2.0
URL:        http://gitlab.cern.ch/cloud-infrastructure/python-landbclient
Source0:    https://pypi.python.org/packages/source/p/%{name}/%{name}-%{version}.tar.gz

BuildArch:  noarch

BuildRequires: python2-devel
BuildRequires: python-setuptools
BuildRequires: python-requests
BuildRequires: python-novaclient
BuildRequires: python-neutronclient

Requires: python-cliff >= 1.0
Requires: python-configparser >= 3.5
Requires: python2-keystoneauth1 >= 2.12
Requires: python2-novaclient >= 9.1.1
Requires: python2-neutronclient >= 6.7.0
Requires: python-requests >= 2.6
Requires: python-six >= 1.7.2
Requires: python-suds >= 0.4
Requires: python2-osc-lib
Requires: python2-os-client-config

%description
Client library and command line utility for interacting with CERN's landb.

%prep
%setup -q -n %{name}-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# Install other needed files
install -p -D -m 644 %{_builddir}/%{name}-%{version}/landb.bash_completion \
    %{buildroot}%{_sysconfdir}/bash_completion.d/landb.bash_completion

%files
%doc LICENSE
%doc README.md
%{_bindir}/landb
%{python_sitelib}/landbclient
%{python_sitelib}/*.egg-info
%{_sysconfdir}/bash_completion.d/landb.bash_completion

%changelog
* Wed Feb 26 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 0.19.1-1
- Add python3 compatibility

* Wed Jan 15 2020 Jose Castro Leon <jose.castro.leon@cern.ch> 0.19.0-1
- Add serialno search while querying devices
- Add tag search while querying devices

* Fri Mar 01 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 0.18.1-1
- Add param region name and use it on the openstack clients

* Fri Mar 01 2019 Ricardo Rocha <ricardo.rocha@cern.ch> 0.17.2-1
- Add prefix routers as a param to the command line

* Tue Aug 28 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.17.1-1
- Revert device_hostname returning a single value to a list of values

* Thu Aug 23 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.17.0-1
- Fix device_hostname should return one value only

* Wed Aug 22 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.16.0-1
- Support session configuration OpenStack envs

* Tue Aug 21 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.15.0-1
- Use neutron tags instead of cellipsrv mapping file for consistency chek

* Mon Jul 16 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.14.2-2
- Set suds dependency to python-suds >= 0.4 to avoid ai-tools conflicts

* Fri Apr 20 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.14.2-1
- Fix index out of range error in VMs without interface attached

* Wed Apr 18 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.14.1-1
- Add cell name and timestamp to error msg on landb check

* Wed Mar 07 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.14.0-1
- Add ipv6ready flag metadata update

* Thu Mar 01 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.13.1-1
- Fix set_alias adding domain_name when needed

* Wed Feb 07 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.13.0-1
- Allow pass alias parameter during the vm creation

* Wed Jan 31 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.12.2-1
- Exclude router devices from landb consistency

* Tue Jan 30 2018 Iago Santos <iago.santos.pardo@cern.ch> 0.12.1-1
- Fix variable error msg

* Thu Jan 18 2018 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.0-3
- Fix dependency version on keystoneauth1

* Wed Jan 17 2018 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.0-2
- Fix dependency on keystoneauth1

* Wed Jan 17 2018 Ricardo Rocha <ricardo.rocha@cern.ch> 0.12.0-1
- Add check command for nova and landb consistency

* Wed Dec 20 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.11.0-1
- Add surname argument to device_search

* Thu Dec 14 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.10.0-1
- Fix return dev-search to a list of devices

* Tue Dec 05 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.9.1-1
- Pass proper parameter ip_addr on device_search

* Tue Nov 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.9.0-1
- Add support for vm-migrate command

* Tue Nov 28 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.8.0-1
- Set vm_delete to handle multiples devices

* Wed Oct 25 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.8-1
- Make configurable vm-delete destroy and set it by default to True

* Fri Sep 29 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.7-1
- Cleanup device when add interface fails

* Tue Aug 08 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.6-1
- Allow passing domain name on vm creation

* Tue Aug 01 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.5-2
- Add info how to run tests README

* Tue Aug 01 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.5-1
- Add dependency python-configparser

* Mon Jul 31 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.4-1
- Load params from landb.conf

* Thu Jul 27 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.3-1
- Fix ipv6 and landb SOAPv6 usage

* Mon Jul 10 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.2-2
- Check version match of main.py and spec on gitlab-ci before_script

* Tue Jun 27 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.2-1
- Add required env vars in gitlab-ci before_script

* Tue Jun 27 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.1-1
- Bump version to qa build procedure

* Tue Jun 27 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.7.0-1
- Add command service-info

* Tue Jun 27 2017 Iago Santos <iago.santos.pardo@cern.ch> 0.6.0-1
- Add support to landb SOAP V6

* Tue Apr 11 2017 Ricardo Rocha <ricardo.rocha@cern.ch> 0.5.0-1
- Add support for multiple interface management in landb

* Tue Apr 11 2017 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.4-1
- Reuse device_search in device_hostname
- Fix unit tests

* Mon Jun 06 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.3-4
- Fix suds version (0.7)

* Tue May 24 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.3-2
- Rebuild for new gitlab-ci workflow

* Wed Apr 27 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.3-3
- Set suds dependency to python2-suds >= 0.7.0

* Wed Apr 20 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.3-1
- Update for new release

* Tue Apr 19 2016 Davide Michelino <davide.michelino@cern.ch> 0.4.2-1
- Update for new release

* Thu Jan 28 2016 Davide Michelino <davide.michelino@cern.ch> 0.4.1-1
- Update for new release

* Fri Jan 22 2016 Ricardo Rocha <ricardo.rocha@cern.ch> 0.4.0-2
- Release testing gitlab ci integration
- Drop %{?dist} from Release

* Tue Dec 15 2015 Davide Michelino <davide.michelino@cern.ch> 0.4.0-1
- Update for new release

* Wed Oct 21 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.3.4-1
- Update for new release

* Wed Oct 07 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.3.3-1
- Update for new release

* Tue Sep 15 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.3.2-1
- Update for new release
- Back to 1.7.2 on python-six dependency

* Wed Sep 09 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.3.1-1
- Update for new release
- Add python-six dependency (1.9.0)

* Tue Sep 08 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.3.0-1
- Update for new release

* Mon Aug 31 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.2.0-1
- Update for new release

* Wed Aug 26 2015 Ricardo Rocha <ricardo.rocha@cern.ch> 0.1.0-1
- First release
