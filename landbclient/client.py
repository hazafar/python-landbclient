# Copyright 2018 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

"""Client for the CERN landb.

This module offers an API to interact with the CERN landb.
"""

import logging
import random
import string
import suds.client
import suds.wsdl
import suds.xsd.doctor

from keystoneauth1 import session as keystone_session
from keystoneauth1.identity import v3
from neutronclient.v2_0 import client as neutron_client
from novaclient import client as nova_client

from osc_lib.api import auth
from os_client_config import config as cloud_config
from landbclient.transport import RequestsTransport
from landbclient import errors


# pylint: disable=R0201
class LanDB(object):
    """
    LanDB is a python client on top of the CERN's landb SOAP interface.
    """

    DEFAULT_NAMEMAXTRIES = 5
    DEFAULT_NAMESIZE = 10
    DEFAULT_MAXIPSERVICE = 255
    DEFAULT_NAMESTART = 'Z'
    LANDB_MAX_REQ_SIZE = 200

    log = logging.getLogger(__name__)

    def __init__(self, username, password, host, port,
                 cert_file=None, key_file=None, ca_file=None, protocol='https',
                 version=5, os_auth_url='https://keystone.cern.ch/main/v3',
                 os_username=None, os_password=None,
                 os_project_name=None, os_auth_version=2,
                 os_region_name='cern', prefix_routers=[]):
        " Initializes a landb client with the given params. "
        url = "%s://%s:%s" % (protocol, host, port)
        imp = suds.xsd.doctor.Import(
            'http://schemas.xmlsoap.org/soap/encoding/')
        try:
            self.client = suds.client.Client(
                "%s/sc/soap/soap.fcgi?v=%d&WSDL" % (url, version),
                doctor=suds.xsd.doctor.ImportDoctor(imp),
                transport=RequestsTransport(cert_file, key_file, ca_file))
            self.__auth(username, password)
        except Exception:
            # fallback for mock landb service compatibility
            try:
                self.client = suds.client.Client(
                    "%s/axis/services/NetworkServicePort?wsdl" % url,
                    doctor=suds.xsd.doctor.ImportDoctor(imp),
                    transport=RequestsTransport(
                        cert_file, key_file, ca_file))
                self.__auth(username, password)
            except Exception as exc:
                raise Exception(
                    "failed fetch wsdl from %s (check passwd?) :: %s"
                    % (url, exc))

        self.soap = self.client.service
        self.prefix_routers = prefix_routers

        try:
            sess = self._make_session(os_auth_url,
                                      os_username,
                                      os_password,
                                      os_project_name)
        except Exception as exc:
            raise Exception(
                "failed to create session :: %s" % exc)

        self.nova = nova_client.Client(os_auth_version, session=sess,
                                       region_name=os_region_name)
        self.neutron = neutron_client.Client(session=sess,
                                             region_name=os_region_name)

    def _make_session(self, os_auth_url, os_username, os_password,
                      os_project_name):
        """Create our base session using simple auth from ksc plugins
        The arguments required in opts varies depending on the auth plugin
        that is used.  This example assumes Identity v2 will be used
        and selects token auth if both os_url and os_token have been
        provided, otherwise it uses password.
        :param opts:
            A parser options containing the authentication
            options to be used
        :param dict kwargs:
            Additional options passed directly to Session constructor
        """
        # If no auth type is named by the user, select one based on
        # the supplied options
        try:
            cconfig = cloud_config.OpenStackConfig()
        except (IOError, OSError) as exc:
            raise Exception(
                "Could not read OpenStackConfig :: %s" % exc)
        env_var = False
        try:
            cloud = cconfig.get_one_cloud()
            env_var = True
        except Exception:
            self.log.debug("environment credentials not found, \
                           switching to landb config file")

        if env_var:
            auth_plugin_name = cloud.config['auth_type']
            auth.check_valid_authentication_options(
                cloud,
                auth_plugin_name, )
            auth_p = cloud.get_auth()
        else:
            auth_p = v3.Password(auth_url=os_auth_url,
                                 username=os_username,
                                 password=os_password,
                                 project_name=os_project_name,
                                 user_domain_name='default',
                                 project_domain_name='default')
        session = keystone_session.Session(auth=auth_p)
        return session

    def __auth(self, username, password):
        "Get token from landb"
        token = self.client.service.getAuthToken(username, password, 'CERN')
        self.client.set_options(soapheaders=dict(Auth={'token': token}))
        return token

    def device_random(self, maxtries=DEFAULT_NAMEMAXTRIES,
                      namesize=DEFAULT_NAMESIZE, namestart=DEFAULT_NAMESTART):
        " Generates and returns a random non existing device name in landb. "
        for _ in range(maxtries):
            dev_temp = ''.join(
                random.choice(
                    string.ascii_uppercase + string.digits
                ) for x in range(namesize))
            dev_name = namestart + dev_temp
            if not self.device_exists(dev_name):
                return dev_name
        raise Exception("failed to generate a non existing name after %d tries"
                        % maxtries)

    def device_exists(self, name):
        " Returns true if the given device exists in landb. "
        try:
            res = self.client.service.getDeviceInfo(name)
            if res is None:
                return False
        except Exception:
            return False
        return True

    def device_info(self, name):
        " Return info on a device. "
        result = self.client.service.getDeviceInfo(name)
        if not result:
            raise Exception("failed to get device info :: %s" % name)
        return result

    def device_managed(self, name, state='true'):
        " Set or unset if a device is managed in landb. "
        result = self.client.service.vmSetUnsetManagedFlag(name, state)
        if not result:
            raise Exception("failed to set device managed :: %s" % name)

    def device_hostname(self, address):
        " Get the hostname(s) given an IP. "
        result = self.device_search(ip_addr=address)
        return result if result else None

    def device_search(self, ip_addr='', mac='', surname='',
                      serialnumber='', tag=''):
        " Get the hostname given an IP, mac or surname "
        device = self.client.service.searchDevice({
            'IPAddress': ip_addr,
            'HardwareAddress': mac,
            'Surname': surname,
            'SerialNumber': serialnumber,
            'Tag': tag,
        })
        if device:
            return device
        return None

    def device_ipv6ready(self, device, state):
        " Update ipv6 ready flag on the device. "
        return self.client.service.deviceUpdateIPv6Ready(device, state)

    def vm_create(self, vm_name, vm_location, vm_manufacturer, vm_model,
                  vm_os, vm_responsible, vm_mac, vm_cluster, vm_parent,
                  vm_description='', vm_tag='', vm_user=None, vm_ip='',
                  vm_ipv6='', vm_ipservice='', vm_internet=True,
                  vm_landb_manager=None, ipv6ready=True, manager_locked=False,
                  vm_domain_name='cern.ch', vm_alias=[], vm_serialnumber=''):
        """
        Create a vm in landb
        vm_location shall be like {'Floor':'0', 'Room':'0', 'Building':'0'}
        vm_os shall be {'Name':'LINUX', 'Version':'UNKNOWN'}
        vm_responsible and vm_user shall be like:
          {'Name': 'Smith', 'FirstName': 'Alice', 'Department': 'IT',
           'Group': 'OIS', 'PersonID': 'id'}
            Only 'Name' and 'FirstName' are mandatory
        """

        vm_device = {'DeviceName': vm_name,
                     'Location': vm_location,
                     'Manufacturer': vm_manufacturer,
                     'Model': vm_model,
                     'Description': vm_description,
                     'Tag': vm_tag,
                     'OperatingSystem': vm_os,
                     'ResponsiblePerson': vm_responsible,
                     'UserPerson': vm_user,
                     'LandbManagerPerson': vm_landb_manager,
                     'IPv6Ready': ipv6ready,
                     'ManagerLocked': manager_locked,
                     'SerialNumber': vm_serialnumber}

        vm_create_options = {'VMParent': vm_parent}
        vm_interface_name = "%s.%s" % (vm_name, vm_domain_name)

        # add the vm without an interface, in the future will be unlinked
        result = self.client.service.vmCreate(vm_device, vm_create_options)

        if not result:
            raise Exception(
                "failed to create vm :: %s :: %s" % (vm_name, result.fault))

        # add the first interface, attached to the vm above
        try:
            result = self.vm_add_interface(
                vm_name, vm_cluster, vm_mac, vm_ip,
                vm_ipv6, vm_ipservice, vm_interface_name,
                vm_internet=vm_internet)
        except Exception as exc:
            self.vm_delete(vm_name)
            raise exc

        # add alias to the interface
        try:
            for alias in vm_alias:
                self.__set_alias(vm_name, alias)
        except Exception as exc:
            raise exc
        return result

    def vm_add_interface(
            self, vm_device, vm_cluster, vm_mac='', vm_ip='',
            vm_ipv6='', vm_ipservice='', vm_interface_name='Ethernet',
            vm_internet=True):
        """add interface vm in landb"""

        if vm_internet is True:
            vm_internet = 'Y'
        else:
            vm_internet = 'N'

        vm_interface_options = {
            'IP': vm_ip,
            'IPv6': vm_ipv6,
            'ServiceName': vm_ipservice,
            'InternetConnectivity': vm_internet
        }

        result = self.client.service.vmAddCard(
            vm_device, {"HardwareAddress": vm_mac, "CardType": "Ethernet"})
        if not result:
            raise Exception("failed to add interface card :: %s" % vm_device)

        result = self.client.service.vmAddInterface(
            vm_device, vm_interface_name, vm_cluster, vm_interface_options)
        if not result:
            raise Exception("failed to add vm interface :: %s" % vm_device)
        return result

    def vm_remove_interface(self, vm_device, vm_interface_name=''):
        """remove interface vm in landb"""

        result = self.client.service.vmRemoveInterface(
            vm_device, vm_interface_name)

        if not result:
            raise Exception("failed to delete interface :: %s on %s"
                            % (vm_interface_name, vm_device))
        return result

    def vm_update(self, device, new_device=None,
                  location={'Floor': '0', 'Room': '0', 'Building': '0'},
                  manufacter='KVM', model='VIRTUAL MACHINE', description=None,
                  tag='OPENSTACK VM', operating_system=None,
                  responsible_person=None, user_person=None, ipv6ready=None):
        """Update vm metadata in landb"""

        metadata = None
        try:
            metadata = self.client.service.getDeviceBasicInfo(device.upper())
        except Exception:
            pass

        if new_device is None:
            new_device = device

        if description is None and metadata is not None:
            description = metadata.Description \
                    if metadata.Description is not None else ''

        if operating_system is None and metadata is not None:
            operating_system = metadata.OperatingSystem

        if responsible_person is None and metadata is not None:
            responsible_person = metadata.ResponsiblePerson

        if user_person is None and metadata is not None:
            user_person = metadata.UserPerson

        if ipv6ready is None and metadata is not None:
            ipv6ready = metadata.IPv6Ready

        result = self.client.service.vmUpdate(
            device,
            {
                'DeviceName': new_device,
                'Location': location,
                'Manufacturer': manufacter,
                'Model': model,
                'Description': description,
                'Tag': tag,
                'OperatingSystem': operating_system,
                'ResponsiblePerson': responsible_person,
                'UserPerson': user_person,
                'IPv6Ready': ipv6ready}
            )
        return result

    def vm_delete(self, device, destroy=True, new_device=None):
        """
        Delete or mark as delete vms according to destroy flag
        """

        if destroy is True:
            # FIXME: figure out netreset equivalent in soapv6
            # reset = self.client.service.vmNetReset(device)
            result = self.client.service.vmDestroy(device)
        else:
            if new_device is None:
                new_device = self.device_random()

            osystem = {'Name': 'UNKNOWN', 'Version': 'UNKNOWN'}
            responsible = {'FirstName': 'E-GROUP',
                           'Name': 'AI-OPENSTACK-ADMIN',
                           'Department': 'IT'}
            user_person = {'FirstName': 'E-GROUP',
                           'Name': 'AI-OPENSTACK-ADMIN',
                           'Department': 'IT'}
            # FIXME: figure out netreset equivalent in soapv6
            # reset = self.client.service.vmNetReset(device)
            result = self.vm_update(device, new_device=new_device,
                                    description='Not in use',
                                    operating_system=osystem,
                                    responsible_person=responsible,
                                    user_person=user_person)
        # FIXME: figure out netreset equivalent in soapv6
        # if not reset:
        #    raise Exception("failed to reset network on vm :: %s" % device)
        if not result:
            raise Exception("failed to delete vm :: %s" % device)
        return result

    def vm_migrate(self, hostname, node):
        " Migrate vm with hostname to node. "
        self.client.service.vmMigrate(hostname, node)

    def cluster_devices(self, cluster, detail=False):
        " Get all cluster devices. "
        devices = self.client.service.vmClusterGetDevices(cluster)
        if not detail:
            return devices

        device_details, i = [], 0
        while i < len(devices):
            nxt = min(len(devices), i+self.LANDB_MAX_REQ_SIZE)
            device_details.extend(
                self.client.service.getDeviceInfoArray(devices[i:nxt]))
            i = nxt

        result = {}
        for dev in device_details:
            result[dev.DeviceName.lower()] = dev
        return result

    def cluster_info(self, cluster=None, device=None):
        " Get cluster info. "
        if device is not None:
            cluster = self.client.service.vmGetClusterMembership(device)[0]
        else:
            cluster = cluster

        if cluster is None:
            raise Exception("No device or cluster provided, need at least one")

        return self.client.service.vmClusterGetInfo(cluster)

    def ipservice_register(
            self, ipservice, parent,
            location={'Floor': '0', 'Room': '0', 'Building': '0'},
            manufacturer='KVM', model='VIRTUAL MACHINE',
            osystem={'Name': 'UNKNOWN', 'Version': 'UNKNOWN'},
            responsible={'FirstName': 'E-GROUP', 'Name': 'AI-OPENSTACK-ADMIN',
                         'Department': 'IT'},
            mac='02-16-3e',
            maxipsrv=DEFAULT_MAXIPSERVICE):
        " Register a new ip service in landb (fill in with dummy vms). "

        for i in range(1, maxipsrv):
            vmname = self.device_random()
            self.vm_create(
                vmname, location, manufacturer, model, osystem,
                responsible, mac, ipservice, parent, vm_description=vmname,
                vm_tag=vmname, vm_user='mock', vm_ip="192.168.1.%s" % i,
                vm_ipv6="2002::%s" % i, vm_ipservice=ipservice,
                vm_internet=True)
            self.device_managed(vmname)

    def ipservice_info(self, service):
        " Get service information. "
        return self.client.service.getServiceInfo(service)

    def ipservice_devices(self, service):
        " Get devices from service. "
        return self.client.service.getDevicesFromService(service)

    def alias_update(self, device, new_alias):
        """Update alias"""
        try:
            device_info = self.device_info(device)
            old_alias = device_info.Interfaces[0].IPAliases
            if old_alias is None:
                old_alias = []

            for alias in new_alias:
                if (alias not in old_alias) and self.device_exists(alias):
                    pass
                    # FIXME: throw proper raise exception.CernInvalidHostname()

            for alias in old_alias:
                self.__unset_alias(device, alias)

            for alias in new_alias:
                self.__set_alias(device, alias)
        except Exception:
            pass
        # FIXME: throw proper raise exception.CernInvalidHostname()
#        except exception.CernInvalidHostname:
#             msg = _("%s - The device already exists or is not "
#                     "a valid hostname" % str(alias))
#             raise exception.CernInvalidHostname(msg)

    def __set_alias(self, device, alias):
        """Set alias to a device"""
        device_name = device.replace(".cern.ch", "")
        alias_name = alias.replace(".cern.ch", "")
        try:
            self.client.service.interfaceAddAlias(device_name + ".cern.ch",
                                                  alias_name + ".cern.ch")
        except Exception as exc:
            raise Exception("Impossible to set alias %s on device %s :: %s"
                            % (alias_name, device_name, exc))

        # FIXME: throw proper raise exception.CernLanDBUpdate()
#        except Exception as e:
#            raise exception.CernLanDBUpdate()

    def __unset_alias(self, device, alias):
        """Unset all alias in a device"""
        try:
            self.client.service.interfaceRemoveAlias(device, alias)
        except Exception:
            pass
        # FIXME: throw proper raise exception.CernLanDBUpdate()
#            LOG.error(_("Cannot unset alias in landb: %s" % str(e)))
#            raise exception.CernLanDBUpdate()

    def vm_get_cluster_membership(self, hostname):
        """Gets the list of clusters where this physical host is on"""
        host = hostname.split(".")[0]
        return self.client.service.vmGetClusterMembership(host)

    def service_info(self, cluster=None, device=None):
        """
        Get the service info.
        If device is passed, it can be a VM or a hypervisor and the info of all
        services in its cluster are returned.
        If cluster is passed, the info on all its services are returned.
        """

        cluster_info = self.cluster_info(cluster=cluster, device=device)

        if cluster_info is None:
            raise Exception("Cluster %s was not found" % cluster)
        if not cluster_info['Services']:
            raise Exception("Cluster %s has no services" % cluster)

        services = {}
        for service in cluster_info['Services']:
            services[service] = self.client.service.getServiceInfo(service)
        return {'cluster': cluster_info['Name'], 'services': services}


    def check(self, clusters=[], cells=None):
        # pylint: disable=invalid-name
        """
        Check consistency check between nova and landb.
        """
        vms_landb, vms_nova, hypervisors = {}, {}, []

        if cells is not None:
            clusters = self._vms_landb_info(cells, clusters)

        for cluster in clusters:
            hypervisors.extend(self.ipservice_devices(cluster))
            devices, err = [], True
            while err:
                try:
                    devices = self.cluster_devices(
                        "VMPOOL %s" % cluster, detail=True)
                except suds.WebFault as exc:
                    if 'not found in database' in exc:
                        continue
                err = False
            vms_landb.update(devices)

        hypervisors = ["{0}.cern.ch".format(h) for h in hypervisors]
        for hypervisor in hypervisors:
            vms = self.nova.servers.list(search_opts={
                'all_tenants': True, 'host': hypervisor, 'detail': True})
            for entry in vms:
                vms_nova[entry.name.lower()] = entry

        return self._check_vms(vms_landb, vms_nova)


    def _vms_landb_info(self, cells, clusters):
        list_subnets = self.neutron.list_subnets()
        clusters_uuid, cell_mapping, tags = {}, {}, {}

        for list_subnets_item in list_subnets['subnets']:
            clusters_uuid.update({list_subnets_item['name']: list_subnets_item['id']})

        for key_clusters, value_clusters in clusters_uuid.items():
            if self.neutron.show_subnet(str(value_clusters)):
                tags = self.neutron.show_subnet(str(value_clusters))
                if tags['subnet']['tags']:
                    cell_mapping.update({key_clusters: tags['subnet']['tags']})

        for cell in cells:
            for key_mapping, value_mapping in cell_mapping.iteritems():
                if cell in str(value_mapping):
                    clusters.extend([key_mapping])
                    break
            else:
                raise ValueError("%s cell not in given cell_mapping" % cell)

        clusters = [w.replace('VM', 'IP') for w in clusters]
        clusters = [w.replace('-V6', '') for w in clusters]

        return list(set(clusters))


    def _check_vms(self, vms_landb, vms_nova):
        # pylint: disable=invalid-name
        """
        Check and compare the given landb and nova vm lists.
        """
        result = []

        vms_landb_set = set([v.lower() for v in vms_landb.keys()])
        vms_nova_set = set([v.lower() for v in vms_nova.keys()])

        for vm in vms_landb_set - vms_nova_set:
            if vm.startswith(tuple(self.prefix_routers)):
                continue
            timestamp = vms_landb[vm].LastChangeDate.TimeUTC.ctime()
            result.append(errors.MissingNovaError(vm=vm, timestamp=timestamp))
            del vms_landb[vm]
        for vm in vms_nova_set - vms_landb_set:
            result.append(errors.MissingLandbError(vm=vm))
            del vms_nova[vm]

        for vm in vms_nova.keys():
            try:
                landb_iface = vms_landb[vm].Interfaces[0]
                nova_iface = vms_nova[vm].addresses['CERN_NETWORK']
                if nova_iface:
                    if landb_iface.IPAddress != nova_iface[0]['addr']:
                        result.append(errors.WrongInfoError(
                            vm=vm, landb=landb_iface, nova=nova_iface))
                    if len(nova_iface) > 1 and landb_iface.IPv6Address != nova_iface[1]['addr']:
                        result.append(errors.WrongInfoError(
                            vm=vm, landb=landb_iface, nova=nova_iface))
                else:
                    result.append(errors.MissingInterfaceError(vm=vm))
            except Exception as exc:
                result.append(
                    errors.LandbError(
                        vm=vm, message="failed to check %(vm)s : %(e)s", e=exc)
                    )

        return result
