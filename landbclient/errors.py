# Copyright 2018 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# pylint: disable=invalid-name

"""Errors for landbclient."""

class LandbError(Exception):
    """Generic landb client error."""
    message = ""
    vm = None

    def __init__(self, vm=None, message=None, **kwargs):
        super(LandbError, self).__init__(message)
        self.vm = vm
        self.kwargs = kwargs
        self.kwargs['vm'] = vm
        if message:
            self.message = message
        self.message = self.message % kwargs

    def __str__(self):
        return self.message

    def __unicode__(self):
        return self.message


class MissingLandbError(LandbError):
    """Entry missing on landb."""
    message = "%(vm)s existing in nova and missing in landb"


class MissingNovaError(LandbError):
    """Entry missing in nova."""
    message = "%(vm)s existing in landb and missing in nova :" \
            " last modified %(timestamp)s"


class WrongInfoError(LandbError):
    """Inconsistent or wrong info between landb and nova."""
    message = "%(vm)s info mismatch : nova is %(nova)s : landb is %(landb)s"


class MissingInterfaceError(LandbError):
    """Missing interface in nova instance."""
    message = "missing interface in %(vm)s"
