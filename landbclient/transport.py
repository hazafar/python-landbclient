"""Requests transport implementation for the landb client.

Allows us to reconfigure things like certificates, logging, etc.
"""

import io
import logging
from suds.transport.http import HttpAuthenticated
from suds.transport import Reply
import requests


class RequestsTransport(HttpAuthenticated):
    """Transport implementation to reconfigure logging level."""

    def __init__(self, cert_file=None, key_file=None, ca_file=True):
        self.cert_file = cert_file
        self.key_file = key_file
        self.ca_file = ca_file
        if self.ca_file is None:
            self.ca_file = False

        logging.getLogger('requests').setLevel(logging.CRITICAL)
        logging.getLogger('urllib3').setLevel(logging.CRITICAL)
        HttpAuthenticated.__init__(self)

    def open(self, request):
        """
        Fetches the WSDL using cert.
        """
        self.addcredentials(request)
        resp = requests.get(
            request.url, data=request.message,
            headers=request.headers,
            cert=(self.cert_file, self.key_file),
            verify=self.ca_file)
        result = io.StringIO(resp.content.decode('utf-8'))
        return result

    def send(self, request):
        """
        Posts to service using cert.
        """
        self.addcredentials(request)
        resp = requests.post(request.url, data=request.message,
                             headers=request.headers,
                             cert=(self.cert_file, self.key_file),
                             verify=self.ca_file)
        result = Reply(resp.status_code, resp.headers, resp.content)
        return result
