# Copyright 2015 CERN. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

"""Cliff landb app implementation."""

import getpass
import sys
import os
import configparser

from cliff.app import App
from cliff.commandmanager import CommandManager
from cliff.complete import CompleteCommand
from cliff.help import HelpCommand
from landbclient.client import LanDB


class LandbApp(App):
    """App for the landb cliff client."""

    def __init__(self):
        super(LandbApp, self).__init__(
            description="""
Client for interacting with the CERN landb.
                """,
            version='0.19.0',
            command_manager=CommandManager('landb'),
            )
        self.landb = None

    def build_option_parser(self, description, version, argparse_kwargs=None):

        config_locations = [
            './landb.conf', os.path.expanduser('~/.landb.conf'),
            '/etc/landb.conf']
        config = configparser.ConfigParser()
        config.read(config_locations)

        parser = super(LandbApp, self).build_option_parser(
            description, version, argparse_kwargs)
        parser.add_argument(
            '--landb-user',
            default=False,
            action='store',
            help='landb username',
            )
        parser.add_argument(
            '--landb-password',
            default=False,
            action='store',
            help='landb password',
            )
        parser.add_argument(
            '--landb-host',
            default='network.cern.ch',
            action='store',
            help='landb host (default network.cern.ch)',
            )
        parser.add_argument(
            '--landb-port',
            default='443',
            action='store',
            help='landb port (default 443)',
            )
        parser.add_argument(
            '--cert-file',
            action='store',
            help='certificate to use',
            )
        parser.add_argument(
            '--key-file',
            action='store',
            help='key to use',
            )
        parser.add_argument(
            '--ca-file',
            action='store',
            help='ca to verify',
            )
        parser.add_argument(
            '--protocol',
            default='https',
            action='store',
            help='https or http',
            )
        parser.add_argument(
            '--landb-version',
            default=6,
            type=int,
            action='store',
            help='landb version',
            )
        parser.add_argument(
            '--os-auth-url',
            default='https://keystone.cern.ch/main/v3',
            action='store',
            help='keystone auth url',
            )
        parser.add_argument(
            '--os-username',
            default=False,
            action='store',
            help='nova username',
            )
        parser.add_argument(
            '--os-password',
            default=False,
            action='store',
            help='nova password',
            )
        parser.add_argument(
            '--os-project-name',
            default=False,
            action='store',
            help='tenant name',
            )
        parser.add_argument(
            '--os-auth-version',
            default=2,
            type=int,
            action='store',
            help='session version',
            )
        parser.add_argument(
            '--prefix-routers',
            default=[],
            action='store',
            nargs='*',
            help='router name prefix to ignore on consistency check'
            )
        parser.add_argument(
            '--os-region-name',
            default='cern',
            action='store',
            help='region name',
            )
        parser.add_argument(
            '--trace',
            default=False,
            action='store_true',
            help='print trace of soap request and response',
            )
        if 'default' in config:
            defaults = dict(config.items('default'))
            parser.set_defaults(**defaults)
        return parser

    def initialize_app(self, argv):
        super(LandbApp, self).initialize_app(argv)

    def prepare_to_run_command(self, cmd):
        super(LandbApp, self).prepare_to_run_command(cmd)

        if not isinstance(cmd, HelpCommand) \
                and not isinstance(cmd, CompleteCommand) and not self.landb:
            if not self.options.landb_password:
                self.options.landb_password = getpass.getpass()
            self.landb = LanDB(
                username=self.options.landb_user,
                password=self.options.landb_password,
                host=self.options.landb_host,
                port=self.options.landb_port,
                cert_file=self.options.cert_file,
                key_file=self.options.key_file,
                ca_file=self.options.ca_file,
                protocol=self.options.protocol,
                version=self.options.landb_version,
                os_auth_url=self.options.os_auth_url,
                os_username=self.options.os_username,
                os_password=self.options.os_password,
                os_project_name=self.options.os_project_name,
                os_auth_version=self.options.os_auth_version,
                prefix_routers=self.options.prefix_routers)

    def clean_up(self, cmd, result, err):
        super(LandbApp, self).clean_up(cmd, result, err)

    def run(self, argv):
        result = super(LandbApp, self).run(argv)
#        if self.options.trace:
#            try:
#                self.stdout.write(
#                    "\n========== REQUEST ==========\n %s\n"
#                    % self.landb.client.last_sent())
#                self.stdout.write(
#                    "\n========== RESPONSE ==========\n %s\n"
#                    % self.landb.client.last_received())
#            except Exception:
#                self.stdout.write("SOAP trace not available\n")
        return result


def main(argv=sys.argv[1:]):
    """Entrypoint for the landbclient."""
    landb = LandbApp()
    return landb.run(argv)

if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
