#!/usr/bin/env python
#!/usr/bin/env python

PROJECT = 'landb'

# Change docs/sphinx/conf.py too!
VERSION = '0.4.3'

from setuptools import setup, find_packages

try:
    long_description = open('README.md', 'r').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,

    description='CLI and Client for CERN landb',
    long_description=long_description,

    author='CERN Cloud',
    author_email='cloud-developers@cern.ch',

    url='https://gitlab.cern.ch/cloud-infrastructure/python-landbclient',
    download_url='https://gitlab.cern.ch/cloud-infrastructure/python-landbclient/repository/archive.tar.gz?ref=master',

    classifiers=['License :: OSI Approved :: Apache Software License',
                 'Programming Language :: Python',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.2',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=['cliff', 'suds-jurko', 'requests'],

    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,

    entry_points={
        'console_scripts': [
            'landb = landbclient.main:main'
        ],
        'landb': [
            'check = landbclient.command:Check',
            'cluster-dev = landbclient.command:ClusterDevices',
            'cluster-info = landbclient.command:ClusterInfo',
            'dev-random = landbclient.command:DeviceRandom',
            'dev-exists = landbclient.command:DeviceExists',
            'dev-managed = landbclient.command:DeviceManaged',
            'dev-info = landbclient.command:DeviceInfo',
            'dev-search = landbclient.command:DeviceSearch',
            'dev-setip6 = landbclient.command:DeviceIPv6',
            'ipsrv-register = landbclient.command:IPServiceRegister',
            'soap = landbclient.command:Soap',
            'vm-create = landbclient.command:VMCreate',
            'vm-add-interface = landbclient.command:VMAddInterface',
            'vm-delete = landbclient.command:VMDelete',
            'vm-migrate = landbclient.command:VMMigrate',
            'service-info = landbclient.command:ServiceInfo',
        ],
    },

    test_suite='nose.collector',
    tests_require=['nose'],

    zip_safe=False,
)
